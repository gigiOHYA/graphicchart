package com.chart.example;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.chart.example.chart.model.ChartData;

public class ChartUtilsTest {
	public static final String DEST = "resultTest.jpg";
	private static ArrayList<ChartData[]> datas = new ArrayList<ChartData[]>();

	@BeforeClass
	public static void setUp() {
		Calendar calendar = Calendar.getInstance();
		for (int i = 0; i < 10; i++) {
			calendar = Calendar.getInstance();
			ChartData[] chartDatas = getRandomChartData(10, 50, calendar);
			datas.add(chartDatas);
		}
	}

	@Test
	public void test() {
		int width = 800;
		int height = 600;
		List<Integer> colors = ChartUtils.drawChartGraphic(new File(DEST), width, height, 15, 45, datas);
		assertEquals(datas.size(), colors.size());
	}

	private static ChartData[] getRandomChartData(int minValue, int maxValue, Calendar calendar) {
		ChartData[] chartDatas = new ChartData[10];
		for (int i = 0; i < 10; i++) {
			calendar.add(Calendar.HOUR, i);
			chartDatas[i] = new ChartData(calendar.getTime(),
					(float) ((Math.random() * (maxValue - minValue + 1)) + minValue));
		}
		return chartDatas;
	}

}
