package com.chart.example.chart;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.chart.example.chart.model.BeizeCurveFormula;
import com.chart.example.chart.model.BeizeCurveFormulaPoint;
import com.chart.example.chart.model.ChartData;
import com.chart.example.chart.model.ChartPoint;

/**
 * Chart that draws ChartData list, upper/lower line, indicates over limit area
 * 
 * @author gigichien
 *
 */
public class GraphicChart {
	private static Logger log = Logger.getLogger(GraphicChart.class.getName());
	private Graphics2D graphic2D;
	private final static int NUM_ROW = 5;
	private final static int NUM_COL = 5;
	private Color limitAreaColor = new Color(255, 230, 230);
	private Color limitLineColor = new Color(100, 30, 30);

	private float margin = 100;
	private float marginLabel = 10;

	private float chartWidth = 600;
	private float chartHeight = 200;

	private float spacingCol;
	private float spacingRow;

	private float dataGapX;
	private float dataGapY;
	private float maxX;
	private float maxY;
	private float minX;
	private float minY;

	private float upperLimit;
	private float lowerLimit;
	private float limitLowerPositionY;
	private float limitUpperPositionY;
	private Font currentFont;

	private ArrayList<ChartDataPaintInfo> chartDataPaints = new ArrayList<ChartDataPaintInfo>();

	public GraphicChart(Graphics graphic) {
		this.graphic2D = (Graphics2D) graphic;
	}

	public void drawData(int width, int height, float limitLower, float limitUpper, List<ChartData[]> chartDatas) {
		currentFont = graphic2D.getFont();
		graphic2D.setBackground(Color.WHITE);
		graphic2D.clearRect(0, 0, width, height);
		lowerLimit = limitLower;
		upperLimit = limitUpper;
		margin = getLabelWidth("0000.0000");
		chartWidth = width - (margin * 2);
		chartHeight = height - (margin * 2) - (currentFont.getSize2D() * 2);
		setData(chartDatas);
		drawChart();
	}

	public List<Integer> getDataColors() {
		List<Integer> colors = new ArrayList<Integer>();
		for (ChartDataPaintInfo chartDataPaint : chartDataPaints) {
			colors.add(chartDataPaint.color);
		}
		return colors;
	}

	private void setData(List<ChartData[]> chartDatas) {
		this.chartDataPaints.clear();
		int colors[] = getRandomColor(chartDatas.size());
		int count = 0;
		for (ChartData[] chartData : chartDatas) {
			ChartDataPaintInfo chartDataPaint = new ChartDataPaintInfo(chartData);
			chartDataPaint.color = colors[count++];
			chartDataPaints.add(chartDataPaint);
		}
	}

	private void drawChart() {
		graphic2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		spacingCol = chartWidth / NUM_COL;
		spacingRow = chartHeight / NUM_ROW;
		calcDataInfo();
		drawLabels();
		setFormulaPerameters();
		drawLimitArea();
		drawForm();
		drawChartWave();
		drawLimitLine();
		graphic2D.dispose();
	}

	private void calcDataInfo() {
		List<Long> timeData = new ArrayList<Long>();
		List<Long> temperatureData = new ArrayList<Long>();
		for (ChartDataPaintInfo data : chartDataPaints) {
			for (ChartData chartData : data.chartDatas) {
				timeData.add(chartData.date.getTime());
				temperatureData.add((long) chartData.value);
			}
		}
		maxX = findMax(timeData);
		maxY = findMax(temperatureData) + 10;
		minX = findMin(timeData);
		minY = findMin(temperatureData) - 10;
		dataGapX = (maxX - minX) / NUM_COL;
		dataGapY = (maxY - minY) / NUM_ROW;

		limitLowerPositionY = (((maxY - lowerLimit) / (maxY - minY)) * (chartHeight)) + margin;
		limitUpperPositionY = (((maxY - upperLimit) / (maxY - minY)) * (chartHeight)) + margin;
	}

	private void setFormulaPerameters() {
		for (ChartDataPaintInfo chartDataPaint : chartDataPaints) {
			ChartData[] datas = chartDataPaint.chartDatas;
			for (int i = 0; i < datas.length; i++) {
				ChartPoint point = new ChartPoint(
						(((datas[i].date.getTime() - minX) / (maxX - minX)) * (chartWidth)) + margin,
						(((maxY - datas[i].value) / (maxY - minY)) * (chartHeight)) + margin);
				chartDataPaint.chartPoints.add(point);
				log.info("mappingChartPoints " + i + ", " + point.x + ", " + point.y);
			}
			for (int i = 0; i < chartDataPaint.chartPoints.size(); i++) {
				log.info("mapping total " + i + ", " + chartDataPaint.chartPoints.get(i).x + ", "
						+ chartDataPaint.chartPoints.get(i).y);
			}
			chartDataPaint.curveFormulaPoints = getCurveFormulaPoints(chartDataPaint.chartPoints);
			chartDataPaint.calcCurvePath();
		}
	}

	private void drawLimitRetangleArea() {
		for (ChartDataPaintInfo chartDataPaint : chartDataPaints) {
			chartDataPaint.setLimitData(upperLimit, lowerLimit);
			log.info("size " + chartDataPaint.lowerLimitIndex.size());
			for (int[] limitIndexs : chartDataPaint.upperLimitIndex) {
				ChartPoint xStart;
				if (chartDataPaint.getChartData(limitIndexs[0]).value >= upperLimit) {
					xStart = chartDataPaint.chartPoints.get(limitIndexs[0]);
				} else {
					xStart = findPointOnCubicBezier(chartDataPaint.curveFormulaPoints.get(limitIndexs[0]),
							limitUpperPositionY);
				}
				ChartPoint xEnd;
				if (limitIndexs[1] == chartDataPaint.chartDatas.length - 1) {
					xEnd = chartDataPaint.chartPoints.get(limitIndexs[1]);
				} else if (chartDataPaint.getChartData(limitIndexs[1] + 1).value >= upperLimit) {
					xEnd = chartDataPaint.chartPoints.get(limitIndexs[1] + 1);
				} else {
					xEnd = findPointOnCubicBezier(chartDataPaint.curveFormulaPoints.get(limitIndexs[1]),
							limitUpperPositionY);
				}
				graphic2D.setColor(limitAreaColor);
				log.info("over limit " + ", " + xStart.x + ", " + xEnd.x);
				graphic2D.fillRect((int) xStart.x, (int) margin, (int) (xEnd.x - xStart.x), (int) chartHeight);
			}

			for (int[] limitIndexs : chartDataPaint.lowerLimitIndex) {
				ChartPoint xStart;
				if (chartDataPaint.getChartData(limitIndexs[0]).value <= lowerLimit) {
					xStart = chartDataPaint.chartPoints.get(limitIndexs[0]);
				} else {
					xStart = findPointOnCubicBezier(chartDataPaint.curveFormulaPoints.get(limitIndexs[0]),
							limitLowerPositionY);
				}
				ChartPoint xEnd;
				if (limitIndexs[1] == chartDataPaint.chartDatas.length - 1) {
					xEnd = chartDataPaint.chartPoints.get(limitIndexs[1]);
				} else if (chartDataPaint.getChartData(limitIndexs[1] + 1).value <= lowerLimit) {
					xEnd = chartDataPaint.chartPoints.get(limitIndexs[1] + 1);
				} else {
					xEnd = findPointOnCubicBezier(chartDataPaint.curveFormulaPoints.get(limitIndexs[1]),
							limitLowerPositionY);
				}

				graphic2D.setColor(limitAreaColor);
				log.info("below limit " + ", " + limitIndexs[0] + ", " + limitIndexs[1] + ": " + xStart.x + ", "
						+ xEnd.x);
				graphic2D.fillRect((int) xStart.x, (int) margin, (int) (xEnd.x - xStart.x), (int) chartHeight);
			}
		}
	}

	private void drawLimitArea() {
		for (ChartDataPaintInfo chartDataPaint : chartDataPaints) {
			GeneralPath upperPath = (GeneralPath) chartDataPaint.chartCurvePath.clone();
			GeneralPath lowerPath = (GeneralPath) chartDataPaint.chartCurvePath.clone();

			lowerPath.lineTo(chartWidth + margin, limitLowerPositionY);
			lowerPath.lineTo(margin, limitLowerPositionY);
			graphic2D.setColor(limitAreaColor);
			graphic2D.fill(lowerPath);

			upperPath.lineTo(chartWidth + margin, limitUpperPositionY);
			upperPath.lineTo(margin, limitUpperPositionY);
			log.info("paint color" + chartDataPaint.color);
			graphic2D.setColor(limitAreaColor);
			graphic2D.fill(upperPath);
		}
		graphic2D.clearRect((int) margin, (int) limitUpperPositionY, (int) chartWidth,
				(int) (limitLowerPositionY - limitUpperPositionY + 1));
	}

	private void drawChartWave() {
		for (ChartDataPaintInfo chartDataPaint : chartDataPaints) {
			GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
			ChartPoint startPoint = chartDataPaint.chartPoints.get(0);
			log.info("start " + ", " + startPoint.x + ", " + startPoint.y);
			path.moveTo(startPoint.x, startPoint.y);
			for (int i = 0; i < chartDataPaint.curveFormulaPoints.size(); i++) {
				BeizeCurveFormulaPoint curveFormulaPoint = chartDataPaint.curveFormulaPoints.get(i);
				path.curveTo(curveFormulaPoint.controllerPoint1.x, curveFormulaPoint.controllerPoint1.y,
						curveFormulaPoint.controllerPoint2.x, curveFormulaPoint.controllerPoint2.y,
						curveFormulaPoint.endPoint.x, curveFormulaPoint.endPoint.y);
			}

			log.info("paint color" + chartDataPaint.color);
			graphic2D.setColor(new Color(chartDataPaint.color));
			graphic2D.draw(path);
		}
	}

	private void drawLabels() {
		String[] labelX = new String[NUM_COL];
		String[] labelY = new String[NUM_ROW];

		Font currentFont = graphic2D.getFont();
		Font newFont = currentFont.deriveFont(currentFont.getSize() * 1f);
		graphic2D.setFont(newFont);
		for (int i = 0; i < labelX.length + 1; i++) {
			float time = minX + (dataGapX * i);
			String date = new SimpleDateFormat("yyyy.MM.dd").format(time);
			String timestamp = new SimpleDateFormat("HH:mm:ss").format(time);
			graphic2D.setColor(Color.BLACK);

			graphic2D.drawString(date,
					((chartWidth / NUM_COL) * i) + margin - (graphic2D.getFontMetrics().stringWidth(date) / 2),
					chartHeight + margin + newFont.getSize2D() + marginLabel);
			graphic2D.drawString(timestamp,
					((chartWidth / NUM_COL) * i) + margin - (graphic2D.getFontMetrics().stringWidth(timestamp) / 2),
					chartHeight + margin + newFont.getSize2D() + marginLabel + (currentFont.getSize()));
		}

		for (int i = 0; i < labelY.length; i++) {
			labelY[i] = String.format("%.2f", (maxY - (dataGapY * i)));
			graphic2D.setColor(Color.BLACK);
			graphic2D.drawString(labelY[i], margin - (graphic2D.getFontMetrics().stringWidth(labelY[i])) - marginLabel,
					margin + (((chartHeight / NUM_ROW)) * i) + (getLabelHeight(labelY[i]) / 2));

		}
	}

	private void drawLimitLine() {
		Line2D line2d = new Line2D.Float(margin, limitLowerPositionY, chartWidth + margin, limitLowerPositionY);
		graphic2D.setColor(limitLineColor);
		graphic2D.draw(line2d);
		line2d = new Line2D.Float(margin, limitUpperPositionY, chartWidth + margin, limitUpperPositionY);
		graphic2D.setColor(limitLineColor);
		graphic2D.draw(line2d);
	}

	private ArrayList<BeizeCurveFormulaPoint> getCurveFormulaPoints(ArrayList<ChartPoint> chartPoints) {
		ArrayList<BeizeCurveFormulaPoint> curveFormulaPoints = new ArrayList<BeizeCurveFormulaPoint>();
		for (int i = 0; i < chartPoints.size() - 1; i++) {
			ChartPoint sp = chartPoints.get(i);
			ChartPoint ep = chartPoints.get(i + 1);
			ChartPoint c1 = new ChartPoint((sp.x + ep.x) / 2, sp.y);
			ChartPoint c2 = new ChartPoint((sp.x + ep.x) / 2, ep.y);
			curveFormulaPoints.add(new BeizeCurveFormulaPoint(sp, c1, c2, ep));
		}
		return curveFormulaPoints;
	}

	private void drawForm() {
		// draw form
		for (int i = 0; i <= NUM_COL; i++) {
			Line2D line2d = new Line2D.Float((i * spacingCol) + margin, margin, (i * spacingCol) + margin,
					chartHeight + margin);
			graphic2D.setColor(Color.LIGHT_GRAY);
			graphic2D.draw(line2d);
		}
		for (int i = 0; i <= NUM_ROW; i++) {
			Line2D line2d = new Line2D.Float(margin, (i * spacingRow) + margin, chartWidth + margin,
					(i * spacingRow) + margin);
			graphic2D.setColor(Color.LIGHT_GRAY);
			graphic2D.draw(line2d);
		}
	}

	private long findMax(List<Long> data) {
		long max;
		max = data.get(0);
		for (int i = 1; i < data.size(); i++) {
			if (data.get(i) > max) {
				max = data.get(i);
			}
		}
		return max;
	}

	private long findMin(List<Long> data) {
		long min;
		min = data.get(0);
		for (int i = 1; i < data.size(); i++) {
			if (data.get(i) < min) {
				min = data.get(i);
			}
		}
		return min;
	}

	private ChartPoint findPointOnCubicBezier(BeizeCurveFormulaPoint curveFormulaPoint, float targetY) {
		BeizeCurveFormula curveFormulaX = new BeizeCurveFormula(curveFormulaPoint.startPoint.x,
				curveFormulaPoint.controllerPoint1.x, curveFormulaPoint.controllerPoint2.x,
				curveFormulaPoint.endPoint.x);
		BeizeCurveFormula curveFormulaY = new BeizeCurveFormula(curveFormulaPoint.startPoint.y,
				curveFormulaPoint.startPoint.y, curveFormulaPoint.controllerPoint2.y, curveFormulaPoint.endPoint.y);
		float root = findRoot((int) (curveFormulaPoint.endPoint.x - curveFormulaPoint.startPoint.x), targetY,
				curveFormulaY);
		curveFormulaX.setRoot(root);
		curveFormulaY.setRoot(root);
		ChartPoint result = new ChartPoint(curveFormulaX.getBeizeValue(), curveFormulaY.getBeizeValue());
		return result;
	}

	private float findRoot(int number, float targetY, BeizeCurveFormula curveFormula) {
		float factor = 1.0f / number;
		float root = 0;
		for (int i = 0; i < number - 1; i++) {
			root = factor * i;
			curveFormula.setRoot(root);
			float y = curveFormula.getBeizeValue();
			float nextRoot = factor * (i + 1);
			curveFormula.setRoot(nextRoot);
			float nextY = curveFormula.getBeizeValue();

			if (y == targetY) {
				return root;
			} else if (y < targetY && nextY >= targetY) {
				return nextRoot;
			} else if (y >= targetY && nextY < targetY) {
				return root;
			}
		}
		return root;
	}

	private int getLabelWidth(String label) {
		return graphic2D.getFontMetrics().stringWidth(label);
	}

	private float getLabelHeight(String label) {
		FontRenderContext frc = graphic2D.getFontRenderContext();
		return currentFont.getLineMetrics(label, frc).getHeight();
	}

	private int[] getRandomColor(int number) {
		int[] randomColors = new int[number];
		int interval = 150 / number;
		for (int i = 0; i < randomColors.length; i++) {
			int mod = i % 3;
			int darkInterval = 130 / number;
			int dark = (darkInterval * i);
			int r = mod == 0 ? 255 - (interval * i) : dark;
			int g = mod == 1 ? 255 - (interval * i) : dark;
			int b = mod == 2 ? 255 - (interval * i) : dark;
			randomColors[i] = new Color(r, g, b).getRGB();
		}
		return randomColors;
	}

	private class ChartDataPaintInfo {
		public int color;
		public ChartData[] chartDatas;
		public ArrayList<int[]> upperLimitIndex = new ArrayList<int[]>();
		public ArrayList<int[]> lowerLimitIndex = new ArrayList<int[]>();
		public ArrayList<ChartPoint> chartPoints = new ArrayList<ChartPoint>();
		public GeneralPath chartCurvePath;

		private ArrayList<BeizeCurveFormulaPoint> curveFormulaPoints = new ArrayList<BeizeCurveFormulaPoint>();

		public ChartDataPaintInfo(ChartData[] chartDatas) {
			this.chartDatas = chartDatas;
		}

		public ChartData getChartData(int index) {
			return chartDatas[index];
		}

		public void setLimitData(float overLimit, float belowLimit) {
			upperLimitIndex.clear();
			lowerLimitIndex.clear();
			upperLimitIndex.addAll(getLimitDataIndexList(true, overLimit));
			lowerLimitIndex.addAll(getLimitDataIndexList(false, belowLimit));
		}

		public void calcCurvePath() {
			chartCurvePath = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
			ChartPoint startPoint = chartPoints.get(0);
			log.info("start " + ", " + startPoint.x + ", " + startPoint.y);
			chartCurvePath.moveTo(startPoint.x, startPoint.y);
			for (int i = 0; i < curveFormulaPoints.size(); i++) {
				BeizeCurveFormulaPoint curveFormulaPoint = curveFormulaPoints.get(i);
				chartCurvePath.curveTo(curveFormulaPoint.controllerPoint1.x, curveFormulaPoint.controllerPoint1.y,
						curveFormulaPoint.controllerPoint2.x, curveFormulaPoint.controllerPoint2.y,
						curveFormulaPoint.endPoint.x, curveFormulaPoint.endPoint.y);

			}
		}

		private ArrayList<int[]> getLimitDataIndexList(boolean isOverUpperLimit, float limit) {
			ArrayList<int[]> limitIndexList = new ArrayList<int[]>();
			for (int i = 0; i < chartDatas.length; i++) {
				int chartDataIndex[] = new int[2];
				boolean hasIntersection = (isOverUpperLimit && chartDatas[i].value > limit)
						|| (!isOverUpperLimit && chartDatas[i].value < limit);

				if (hasIntersection) {
					chartDataIndex[0] = (i == 0) ? i : i - 1;
					if (i == chartDatas.length - 1) {
						chartDataIndex[1] = i;
						limitIndexList.add(chartDataIndex);
						log.info("[limit last]  " + chartDataIndex[0] + ", " + chartDataIndex[1]);

					}
					for (int j = i + 1; j < chartDatas.length; j++) {
						boolean hasEndIntersection = (isOverUpperLimit && chartDatas[j].value < limit)
								|| (!isOverUpperLimit && chartDatas[j].value > limit);
						if (hasEndIntersection || j == chartDatas.length - 1) {
							chartDataIndex[1] = j - 1;
							i = j;
							limitIndexList.add(chartDataIndex);
							break;
						}
					}
					log.info("getLimitDataIndexList " + ", " + chartDataIndex[0] + ", " + chartDataIndex[1]);
				}
			}
			return limitIndexList;
		}
	}
}
