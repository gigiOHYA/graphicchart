package com.chart.example.chart.model;
/**
 * BeizeCurveFormulaPoint model
 * @author gigichien
 *
 */
public class BeizeCurveFormulaPoint {
	public final ChartPoint startPoint;
	public final ChartPoint controllerPoint1;
	public final ChartPoint controllerPoint2;
	public final ChartPoint endPoint;

	public BeizeCurveFormulaPoint(ChartPoint sp, ChartPoint c1, ChartPoint c2, ChartPoint ep) {
		this.startPoint = sp;
		this.controllerPoint1 = c1;
		this.controllerPoint2 = c2;
		this.endPoint = ep;
	}
}