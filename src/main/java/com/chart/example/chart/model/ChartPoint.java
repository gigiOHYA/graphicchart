package com.chart.example.chart.model;

/**
 * ChartPoint 
 * @author gigichien
 *
 */
public class ChartPoint {
	public float x;
	public float y;

	public ChartPoint(float x, float y) {
		this.x = x;
		this.y = y;
	}
}
