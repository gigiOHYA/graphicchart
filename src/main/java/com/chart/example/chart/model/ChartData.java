package com.chart.example.chart.model;

import java.util.Date;

/**
 * ChartData model
 * @author gigichien
 *
 */
public class ChartData {
	public final Date date;
	public final float value;

	public ChartData(Date date, float value) {
		this.date = date;
		this.value = value;
	}
}